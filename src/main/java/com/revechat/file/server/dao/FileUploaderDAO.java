package com.revechat.file.server.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Repository;

import com.revechat.file.server.dto.FileUploadDTO;

import databasemanager.DatabaseManager;

@Repository
public class FileUploaderDAO {

	public void deleteFileFormConversation(String accountId, String servingId, String fileName) {

		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			connection = DatabaseManager.getInstance().getConnection();

			String sql = "SELECT msg FROM vbmessage_reformed WHERE servingID = ?;";

			ps = connection.prepareStatement(sql);
			ps.setString(1, servingId);
			
			rs = ps.executeQuery();

			String message = null;

			if (rs.next()) {
				message = rs.getString("msg");
			}

			ps.close();
			rs.close();

			if (message != null && !message.equals("")) {

				message = filterMessage(message, fileName);

			}

			sql = "UPDATE vbmessage_reformed SET msg = ? WHERE servingID = ?;";

			ps = connection.prepareStatement(sql);
			ps.setString(1, message);
			ps.setString(2, servingId);

			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				ps.close();
				rs.close();
				DatabaseManager.getInstance().freeConnection(connection);
			} catch (Exception e2) {
			}
		}
	}

	public boolean deleteFile(FileUploadDTO dto) {
		boolean result = true;

		Connection connection = null;
		PreparedStatement ps = null;

		try {

			connection = DatabaseManager.getInstance().getConnection();

			String sql = "DELETE FROM vbFileInformation WHERE accountId = ? AND fileDisplayName = ?;";

			ps = connection.prepareStatement(sql);
			ps.setString(1, dto.getAccountId());
			ps.setString(2, dto.getFileDisplayName());

			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		} finally {
			try {
				ps.close();
				DatabaseManager.getInstance().freeConnection(connection);
			} catch (Exception e2) {
			}
		}

		return result;
	}

	public boolean logFileInformation(FileUploadDTO dto) {
		boolean result = true;

		Connection connection = null;
		PreparedStatement ps = null;

		try {

			connection = DatabaseManager.getInstance().getConnection();

			String sql = "INSERT INTO vbFileInformation(accountId, fileDisplayName, fileUUID, fileSize, timestamp) VALUES(?, ?, ?, ?, ?);";

			ps = connection.prepareStatement(sql);
			ps.setString(1, dto.getAccountId());
			ps.setString(2, dto.getFileDisplayName());
			ps.setString(3, dto.getFileUUID());
			ps.setLong(4, dto.getFileSize());
			ps.setLong(5, System.currentTimeMillis());

			ps.execute();

		} catch (Exception e) {
			e.printStackTrace();
			result = false;
		} finally {
			try {
				ps.close();
				DatabaseManager.getInstance().freeConnection(connection);
			} catch (Exception e2) {
			}
		}

		return result;
	}

	public long getAllocatedDiskSize(String accountId) {

		long result = 0;

		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			connection = DatabaseManager.getInstance().getConnection();

			String sql = "SELECT * from vbdiskspace WHERE accountId = ?;";

			ps = connection.prepareStatement(sql);
			ps.setString(1, accountId);

			rs = ps.executeQuery();

			if (rs.next()) {
				result = rs.getLong("allocated_disk_size");
			}

			result = result * 1024 * 1024;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				ps.close();
				rs.close();
				DatabaseManager.getInstance().freeConnection(connection);
			} catch (Exception e2) {
			}

		}

		return result;
	}

	public long getUsedDiskSize(String accoutId) {
		long result = 0;

		Connection connection = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {

			connection = DatabaseManager.getInstance().getConnection();

			String sql = "SELECT * from vbFileInformation WHERE accountId = ?";

			ps = connection.prepareStatement(sql);
			ps.setString(1, accoutId);

			rs = ps.executeQuery();

			while (rs.next()) {
				result += rs.getLong("fileSize");
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {

			try {
				ps.close();
				rs.close();
				DatabaseManager.getInstance().freeConnection(connection);
			} catch (Exception e2) {
			}

		}
		return result;
	}

	private String filterMessage(String message, String fileName) {

		JSONArray jsonArray = new JSONArray(message);

		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject jsonObject = jsonArray.getJSONObject(i);

			if (jsonObject.has("record")) {
				JSONObject recordJson = jsonObject.getJSONObject("record");

				if (recordJson.has("audio")) {
					String dbFileName = recordJson.getString("audio");
					if (dbFileName.equals(fileName)) {
						jsonArray.remove(i);
						break;
					}
				} else if (recordJson.has("video")) {
					String dbFileName = recordJson.getString("video");
					if (dbFileName.equals(fileName)) {
						jsonArray.remove(i);
						break;
					}
				}
			}
		}

		System.out.println(jsonArray.toString());

		return jsonArray.toString();
	}

	public static String DUMMY = "[{\"VName\":\"0001581328\",\"AName\":\"\",\"dir\":0,\"ts\":1581396511753,\"bot-message\":{\"message\":[{\"type\":\"text\",\"message\":{\"text\":\"I am Jahangir API bot, your virtual assistant. How can I help you?\"}}],\"account\":\"12159\",\"sentBy\":\"Agent\",\"timestamp\":1581396511753,\"visitorId\":\"0001581328444458-25900dffffcf96-0002\"}},{\"VName\":\"0001581328\",\"AName\":\"\",\"dir\":0,\"ts\":1581396511753,\"bot-message\":{\"message\":[{\"type\":\"button\",\"message\":{\"attachment\":{\"payload\":{\"buttons\":[{\"payload\":\"2\",\"title\":\"Ok\",\"type\":\"postback\"}],\"template_type\":\"button\",\"text\":\"Please Book an Appointment\"},\"type\":\"template\"}}}],\"account\":\"12159\",\"sentBy\":\"Agent\",\"timestamp\":1581396511753,\"visitorId\":\"0001581328444458-25900dffffcf96-0002\"}},{\"VName\":\"#444580002\",\"AName\":\"Rabby\",\"chat\":\"Welcome message is sent. How may I help you?\",\"dir\":0,\"ts\":1581396524040},{\"voice\":{\"duration\":42908,\"callType\":\"video\"},\"VName\":\"#444580002\",\"AName\":\"Rabby\",\"dir\":0,\"ts\":1581396591759},{\"voice\":{\"duration\":736521,\"callType\":\"video\"},\"VName\":\"#444580002\",\"AName\":\"Rabby\",\"dir\":0,\"ts\":1581397592066},{\"VName\":\"#444580002\",\"AName\":\"Rabby\",\"record\":{\"duration\":10,\"size\":251.35,\"name\":\"video-conversation.webm\",\"video\":\"4015f6cc-bc6c-4eb9-afcf-4c1d96ad1f87.webm\"},\"dir\":0,\"ts\":1581397738480},{\"VName\":\"#444580002\",\"AName\":\"Rabby\",\"record\":{\"duration\":29,\"size\":668.88,\"name\":\"video-conversation.webm\",\"video\":\"0ce9373f-e096-4d8b-b42a-f0f452f3ced7.webm\"},\"dir\":0,\"ts\":1581397902136}]";

	public static void main(String args[]) {
		FileUploaderDAO dao = new FileUploaderDAO();
		dao.filterMessage(DUMMY, "0ce9373f-e096-4d8b-b42a-f0f452f3ced7.webm");
	}
}

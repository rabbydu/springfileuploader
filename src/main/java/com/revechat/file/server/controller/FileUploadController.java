package com.revechat.file.server.controller;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.revechat.file.server.dto.FileUploadDTO;
import com.revechat.file.server.response.Response;
import com.revechat.file.server.service.FileUploaderService;
import com.revechat.file.server.thumbnail.ThumbnailGenerator;

@Controller
public class FileUploadController {

	private static Logger logger = Logger.getLogger(FileUploadController.class);
	//private static final String BASE_PATH = "D:\\upload\\";
	
	private static final String BASE_PATH = "/usr/local/revechat/apps/file-storage/uploads/";

	@Autowired
	public FileUploaderService service;

	@CrossOrigin(origins = "*", maxAge = 36000)
	@RequestMapping(value = { "/", "/upload/", "/upload" }, method = RequestMethod.POST)
	public @ResponseBody Response uploadByFileInputParam(

			@RequestParam(value = "file", required = false) MultipartFile file,
			@RequestParam(value = "file_input", required = false) MultipartFile file_input,
			@RequestParam(value = "datafile", required = false) MultipartFile datafile,
			@RequestParam(value = "myFile", required = false) MultipartFile myFile,
			@RequestParam(value = "accountId", required = false) String accountId,
			@RequestParam(value = "visitorId", required = false) String visitorId

	) throws IOException {

		if (file == null && file_input == null && datafile == null && myFile == null) {
			Response response = new Response();
			response.setReason("Please select a file with valid extension");
			logger.info(
					"file is missing from account id," + accountId + " visitorId," + visitorId + " @" + new Date().toGMTString());
			return response;
		}

		MultipartFile receivedFile = null;

		if (file != null) {

			receivedFile = file;
		} else if (file_input != null) {
			receivedFile = file_input;
		} else if (datafile != null) {
			receivedFile = datafile;
		} else if (myFile != null) {
			receivedFile = myFile;
		}

		logger.info("file uploading request from account id, " + accountId + " for customer id, " + visitorId + " @ "
				+ new Date().toGMTString());

		if (receivedFile == null) {
			Response response = new Response();
			response.setReason("Please select a file with valid extension");
			logger.info(
					"file is missing from account id," + accountId + " visitorId," + visitorId + " @" + new Date().toGMTString());
			return response;
		}

		if (receivedFile.getOriginalFilename().isEmpty()) {
			Response response = new Response();
			response.setReason("Please select a file with valid extension");
			logger.info(
					"file is missing from account id," + accountId + " visitorId," + visitorId + " @" + new Date().toGMTString());
			return response;
		}
		
		byte[] bytes = receivedFile.getBytes();
		long fileSize = bytes.length;
		long fileSizeInKByte = fileSize / 1024;

		long allocatedDiskSize = service.getAllocatedDiskSize(accountId);
		long usedDiskSize = service.getUsedDiskSize(accountId);
		long availableDiskSize = allocatedDiskSize - usedDiskSize;

		if (availableDiskSize <= 0 || fileSizeInKByte > availableDiskSize) {
			Response response = new Response();
			response.setReason("No available stogare.");
			logger.info("no avalilabe storage for account id," + accountId + " visitorId," + visitorId + " @"
					+ new Date().toGMTString());
			return response;
		}

		Response response = new Response();

		String uniqueBaseName = getUniqueId();
		String extension = this.getExtensionName(receivedFile);
		String fileName = uniqueBaseName + extension;
		String filePath = BASE_PATH + fileName;

		BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(new File(BASE_PATH, fileName)));

		
		outputStream.write(bytes);
		outputStream.flush();
		outputStream.close();

		response.setFile(fileName);
		
		FileUploadDTO dto = new FileUploadDTO();
		dto.setAccountId(accountId);
		dto.setFileDisplayName(fileName);
		dto.setFileUUID(uniqueBaseName);
		dto.setFileSize(fileSizeInKByte);
		service.logFileInformation(dto);

		logger.info("trying to download file," + fileName);
		logger.info("file path," + filePath);
		logger.info("file size," + this.humanReadableByteCount(fileSize, false));

		if (isImageExtension(extension)) {

			String thumbname = createThumbnail(filePath, uniqueBaseName, extension, bytes);
			response.setThumb(thumbname);

			logger.info("trying to create thumbnail of image," + thumbname);
		}

		return response;

	}
	
	@CrossOrigin(origins = "*", maxAge = 36000)
	@RequestMapping(value = { "/delete", "/delete/" }, method = RequestMethod.POST)
	public @ResponseBody Response deleteFile(
			@RequestParam(value = "accountId", required = false) String accountId,
			@RequestParam(value = "visitorId", required = false) String visitorId,
			@RequestParam(value = "servingId", required = false) String servingId,
			@RequestParam(value = "fileName", required = false) String fileName,
			HttpServletResponse httpResponse
	) throws IOException {

		logger.info("file deleting request from account id, " + accountId + " @ "
				+ new Date().toGMTString());

		if (fileName == null) {
			httpResponse.setStatus(409);
			Response response = new Response();
			response.setReason("Specify a file name to delete");
			logger.info(
					"file name is missing from account id," + accountId + " visitorId," + visitorId + " @" + new Date().toGMTString());
			return response;
		}
		
		fileName = fileName.trim();

		Response response = new Response();

		String filePath = BASE_PATH + fileName.trim();
		
		File file = new File(filePath);

		if(file.delete())
		{
			FileUploadDTO dto = new FileUploadDTO();
			dto.setAccountId(accountId);
			dto.setFileDisplayName(fileName);
			
			service.deleteFile(dto);
			service.deleteFileFormConversation(accountId, servingId, fileName);
			
			logger.info(file.getName() + " is deleted!");
			response.setFile(fileName);
			response.setSuccess(true);

		}
		else
		{
			logger.info("Delete operation is failed.");
			response.setReason("Delete operation is failed.");
			response.setSuccess(false);
		}


		return response;

	}

	/**
	 * generates and returns a unique string that'll be used to save an uploaded
	 * file to disk
	 *
	 * @return generated unique string
	 */
	private String getUniqueId() {
		UUID uniqueId = UUID.randomUUID();
		return uniqueId.toString();
	}

	public String getExtensionName(MultipartFile file) {

		String[] fileFrags = file.getOriginalFilename().split("\\.");
		String extension = fileFrags[fileFrags.length - 1];
		return "." + extension;
	}

	public String humanReadableByteCount(long bytes, boolean si) {

		int unit = si ? 1000 : 1024;
		if (bytes < unit)
			return bytes + " B";
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = (si ? "kMGTPE" : "KMGTPE").charAt(exp - 1) + (si ? "" : "i");
		return String.format("%.1f %sB", bytes / Math.pow(unit, exp), pre);
	}

	/**
	 * Creates a thumbnail of an image file
	 *
	 * @param fileFullPath  full path of the source image
	 * @param fileNameBase  Base name of the file i.e without extension
	 * @param fileExtension extension of the file
	 */
	private String createThumbnail(String fileFullPath, String fileNameBase, String extension, byte[] bytes) {

		String thumbImgName = fileNameBase + "_thumb" + extension; // thumbnail image base name
		String thumbImageFullPath = BASE_PATH + thumbImgName; // all thumbs are jpg files

		logger.info("fileFullPath:" + fileFullPath);
		try {
			ThumbnailGenerator.transform(fileFullPath, thumbImageFullPath, 100, 100, extension, bytes);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.info("error to create image thumbnail");
			thumbImgName = "";
		}
		return thumbImgName;
	}

	private static boolean isImageExtension(String extension) {
		boolean isImageFile = false;
		String extensionInLowerCase = extension.toLowerCase();

		logger.info("extension:" + extension);

		isImageFile |= extensionInLowerCase.equals(".jpg");
		isImageFile |= extensionInLowerCase.equals(".png");
		isImageFile |= extensionInLowerCase.equals(".jpeg");
		isImageFile |= extensionInLowerCase.equals(".gif");

		return isImageFile;
	}
}

package com.revechat.file.server.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.revechat.file.server.dao.FileUploaderDAO;
import com.revechat.file.server.dto.FileUploadDTO;

@Service
public class FileUploaderService {

	@Autowired
	public FileUploaderDAO dao;
	
	public void deleteFileFormConversation(String accountId, String servingId, String fileName) {
		dao.deleteFileFormConversation(accountId, servingId, fileName);
	}
	
	public boolean deleteFile(FileUploadDTO dto) {
		return dao.deleteFile(dto);
	}
	
	public boolean logFileInformation(FileUploadDTO dto) {
		return dao.logFileInformation(dto);
	}

	public long getAllocatedDiskSize(String accountId) {
		return dao.getAllocatedDiskSize(accountId);
	}

	public long getUsedDiskSize(String accountId) {
		return dao.getUsedDiskSize(accountId);
	}
}
